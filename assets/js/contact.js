$(document).ready(function(){
    
    (function($) {
        "use strict";

    
    jQuery.validator.addMethod('answercheck', function (value, element) {
        return this.optional(element) || /^\bcat\b$/.test(value)
    }, "type the correct answer -_-");

    // validate contactForm form
    $(function() {
        $('#contactForm').validate({
            rules: {
                nama: {
                    required: true,
                    minlength: 2
                },
				notelp:{
					required: true,
                    minlength: 9,
					maxlength:20
				},
				nowa:{
					required: false,
                    minlength: 9,
					maxlength:20
				},
                email: {
                    required: false,
                    email: true
                },
				alamat:{
					required:true,
					minlength: 20
				},
                kota:{
					required:true,
					minlength: 5
				},
				kategori:{
					required:true,
				}
            },
            messages: {
                nama: {
                    required: "Nama anda belum disi",
                    minlength: "nama harus lebih dari 2 karakter"
                },
				notelp: {
                    required: "nomor telefon anda belum disi",
                    minlength: "nomor telefon harus lebih dari 9 karakter",
					maxlength: "nomor telefon lebih dari 15 karakter"
                },
				nowa: {
                    minlength: "nomor WA harus lebih dari 9 karakter",
					maxlength: "nomor telefon lebih dari 15 karakter"
                },
                alamat: {
                    required: "Alamat Belum diisi",
                    minlength: "alamat harus lebih dari 20 karakter"
                },
				kota: {
                    required: "Kota Belum diisi",
                    minlength: "Kota harus lebih dari 5 karakter"
                },
				kategori: {
                    required: "kategori Belum diisi"
                }
            },
            submitHandler: function(form) {
                $(form).ajaxSubmit({
                    type:"POST",
                    data: $(form).serialize(),
                    url: baseurl+"Welcome/sendPemesanan",
                    success: function() {
                        $('#contactForm :input').attr('disabled', 'disabled');
                        $('#contactForm').fadeTo( "slow", 1, function() {
                            $(this).find(':input').attr('disabled', 'disabled');
                            $(this).find('label').css('cursor','default');
                            $('#success').fadeIn()
                            $('.modal').modal('hide');
		                	$('#success').modal('show');
                        })
                    },
                    error: function() {
						console.log($(form).serialize());
                        $('#contactForm').fadeTo( "slow", 1, function() {
                            $('#error').fadeIn()
                            $('.modal').modal('hide');
		                	$('#error').modal('show');
                        })
                    }
                })
            }
        })
    })
        
 })(jQuery)
})