<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct() {
    parent::__construct();
		$this->load->model('M_Welcome');
	}
	//start menu
	public function index(){
		$this->load->view('fix_content/header');
		$this->load->view('dinamis_content/home');
		$this->load->view('fix_content/footer');
	}
	public function tentang(){
		$this->load->view('fix_content/header');
		$this->load->view('dinamis_content/tentang');
		$this->load->view('fix_content/footer');
	}
	public function pemesanan(){
		$data['kategori'] = $this->M_Welcome->getAllKategori()->result();
		$this->load->view('fix_content/header');
		$this->load->view('dinamis_content/pemesanan', $data);
		$this->load->view('fix_content/footer');
	}
	public function galeri(){
		$data['galeri'] = $this->M_Welcome->getAllGaleri()->result();
		$this->load->view('fix_content/header');
		$this->load->view('dinamis_content/galeri', $data);
		$this->load->view('fix_content/footer');
	}
	public function produk($param){
		$paramby=array('galeri.idkategori'=>$param);
		$data['produk'] = $this->M_Welcome->getProdukByKategori($paramby)->result();
		$this->load->view('fix_content/header');
		$this->load->view('dinamis_content/produk', $data);
		$this->load->view('fix_content/footer');
	}

	public function adminHome(){
		if($this->session->userdata('userdata')!=null){
			$data['jml_pemesanan'] = $this->M_Welcome->getAllOrder()->num_rows();
			$data['jml_galeri'] = $this->M_Welcome->getAllGaleri()->num_rows();
			$this->load->view('admin_fix_content/header');
			$this->load->view('admin_dinamis_content/homeAdmin', $data);
			$this->load->view('admin_fix_content/footer');
		}else{
			redirect('Welcome/login');
		}
	}
	public function formLogin(){
		$this->load->view('login');
	}
	public function pesanan(){
		if($this->session->userdata('userdata')!=null){
			$data['pemesanan'] = $this->M_Welcome->getAllOrder()->result();
			$this->load->view('admin_fix_content/header');
			$this->load->view('admin_dinamis_content/pemesanan', $data);
			$this->load->view('admin_fix_content/footer');
		}else{
			redirect('Welcome/login');
		}
	}
	public function adminGaleri(){
		if($this->session->userdata('userdata')!=null){
			$data['galeri'] = $this->M_Welcome->getAllGaleri()->result();
			$data['kategori'] = $this->M_Welcome->getAllKategori()->result();
			$this->load->view('admin_fix_content/header');
			$this->load->view('admin_dinamis_content/galeri', $data);
			$this->load->view('admin_fix_content/footer');
		}else{
			redirect('Welcome/login');
		}
	}
	public function editPemesanan($idPemesanan, $status){
		if($idPemesanan!=null&&$status!=null){
			$where = array(
				'id_pemesanan'=>$idPemesanan
			);
			if($status==1){
				$status='Sedang Diproses';
			}elseif($status==2){
				$status='Selesai';
			}
			$data = array(
				'status_pemesanan'=>$status
			);
				if($this->M_Welcome->editPemesanan($data, $where, 'pemesanan')== 1){
					$this->session->set_flashdata("success", "Data Berhasil Diubah");
				}else{
					$this->session->set_flashdata("failed", "Data Gagal Diubah");
				}
				print_r($this->session->flashdata("success"));
				print_r($this->session->flashdata("failed"));
			redirect('Welcome/pesanan');
		}else{
			redirect('Welcome/pesanan');
		}
	}
	//end menu
	//start auth
	public function login(){
		if($this->session->userdata('userdata')!=null){
			redirect('Welcome/adminHome');
		}else{
			redirect('Welcome/formLogin');
		}
	}
	public function cek_auth(){
		$data = array(
			'username ='=>$this->input->post('username'),
			'password ='=>md5($this->input->post('password'))
		);
		if($this->M_Welcome->cek_auth($data, 'user')!=null){
			$data=$this->M_Welcome->cek_auth($data, 'user')->result();
			foreach($data as $row){
				$user=array(
					'access'=> 'success',
					'nama'=> $row->nama
				);
			}
			$this->session->set_userdata('userdata', $user);
			redirect('Welcome/adminHome');
		}else{
			redirect('Welcome/login');
		}
	}
	public function logout(){
		$this->session->sess_destroy();
		redirect('Welcome/login');
	}
	//end auth
	public function sendPemesanan(){
		date_default_timezone_set("Asia/Bangkok");
		$data = array(
			'nama_pemesan'=>$this->input->post('nama'),
			'no_telp'=>$this->input->post('notelp'),
			'no_wa'=>$this->input->post('nowa'),
			'tgl_pemesanan'=>date("Y-m-d H:i:s"),
			'email'=>$this->input->post('email'),
			'kota'=>$this->input->post('kota'),
			'alamat'=>$this->input->post('alamat'),
			'idkategori'=>$this->input->post('kategori'),
			'status_pemesanan'=>"Belum Diproses"
		);
		$this->M_Welcome->inputPemesanan($data, 'pemesanan');
		if($this->db->affected_rows()){
			redirect('Welcome/pemesanan');
		}else{
			redirect('Error/');
		}
	}
	public function addGaleri(){
		//540x359
		date_default_timezone_set("Asia/Bangkok");
		$config['image_library']='gd2';
		$config['file_name'] = date("Y-m-d-H_i_s").'.jpg';
		$config['upload_path']          = './galeri/';
		$config['allowed_types']        = 'jpg|png';	
		$config['overwrite']			= true;
		$config['max_size']             = 1024;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if (! $this->upload->do_upload('gambar')){
            $error = array('error' => $this->upload->display_errors());
            print_r($error);
        }else{
				$gbr = $this->upload->data();
				$config['image_library']='gd2';
                $config['source_image']='./galeri/';
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                $config['width']= 540;
                $config['height']= 359;
                $config['new_image']= './galeri/';
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
            $data = array(
			'nama'=>$this->input->post('nama'),
			'idkategori'=>$this->input->post('kategori'),
			'tglupload'=>date("Y-m-d H:i:s"),
			'gambar'=>date("Y-m-d-H_i_s").'.jpg'
			);
			$this->M_Welcome->inputGaleri($data, 'galeri');
        }
		redirect('Welcome/adminGaleri');
	}
	public function editGaleri(){
		$where= array(
			'kode'=>$this->input->post('kode')
		);
		
		$config['image_library']='gd2';
		$config['file_name'] = date("Y-m-d-H_i_s").'.jpg';
		$config['upload_path']          = './galeri/';
		$config['allowed_types']        = 'jpg|png';	
		$config['overwrite']			= true;
		$config['max_size']             = 1024;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if($this->upload->do_upload('edit-gambar')==null){
			$data = array(
				'nama'=>$this->input->post('edit-nama')
				);
		}else{
			if (! $this->upload->do_upload('edit-gambar')){
				$error = array('error' => $this->upload->display_errors());
				print_r($error);
			}else{
				$gbr = $this->upload->data();
				$config['image_library']='gd2';
                $config['source_image']='./galeri/';
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                $config['width']= 540;
                $config['height']= 359;
                $config['new_image']= './galeri/';
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
			
				$data = array(
				'nama'=>$this->input->post('edit-nama'),
				'gambar'=>date("Y-m-d-H_i_s").'.jpg'
				);
			}
		}
		$this->M_Welcome->updateGaleri($data, $where,'galeri');
		redirect('Welcome/adminGaleri');
	}
	public function showOneGaleri($id){
		$where= array(
			'kode'=>$id
		);
		$data=$this->M_Welcome->getOneGaleri($where, 'galeri')->result();
		echo json_encode($data);
	}
}
