<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_welcome extends CI_Model{
    public function inputPemesanan($data, $table){
		$this->db->insert($table, $data);
	}
	public function inputGaleri($data, $table){
		$this->db->insert($table, $data);
	}
	public function cek_auth($data, $table){
		return $this->db->get_where($table, $data);
	}
	public function getProdukByKategori($data){
		$this->db->select('*');
		$this->db->from('galeri');
		$this->db->join('kategori', 'galeri.idkategori = kategori.idkategori');
		$this->db->where($data);
		return $query = $this->db->get();
	}
	public function getAllOrder(){
		$this->db->select('*');
		$this->db->from('pemesanan');
		$this->db->join('kategori', 'pemesanan.idkategori = kategori.idkategori');
		return $query = $this->db->get();
	}
	public function getAllGaleri(){
		$this->db->select('*');
		$this->db->from('galeri');
		$this->db->join('kategori', 'galeri.idkategori = kategori.idkategori');
		return $query = $this->db->get();
	}
	public function getAllKategori(){
		return $this->db->get('kategori');
	}
	public function editPemesanan($data, $where, $table){
		$this->db->where($where);
		$this->db->update($table,$data);
		return 1;
	}
	public function getOneGaleri($where, $table){
		return $this->db->get_where($table, $where);
	}
	public function updateGaleri($data, $where, $table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}
}
?>