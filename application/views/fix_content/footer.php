    <!-- ================ start footer Area ================= -->
    <footer class="footer-area">
        <div class="container">
            <div class="row">

                <div class="col-lg-5 col-sm-6 mb-4 mb-xl-0 single-footer-widget">
                    <h4>Tentang Kami</h4>
                    <p>Merupakan bengkel las yang berada di kawasan Matraman Jakarta Timur, Kami menyediakan jasa pembuatan aksesoris rumah seperti canopi, teralis, rolling door, folding gate, pagar, pintu, konstruksi baja dan lain-lain.</p>
                    
                </div>

                <div class="col-lg-4 col-sm-6 mb-4 mb-xl-0 single-footer-widget">
                    <h4>Informasi Kontak</h4>
                    <div class="footer-address">
                        <p>Alamat : Jl. Kelapa Sawit Raya No. 19 RT. 12/8 Kayu Manis Jakarta Timur</p>
                        <span>Telepon : 0858 8868 1718</span>
                        <span>&emsp;&emsp;&emsp;&emsp;&ensp;0812 1926 3118</span>
                        <span>Email : haryajayalas@gmail.com</span>
                    </div>
                </div>

                <div class="col-lg-3 col-md-8 mb-4 mb-xl-0 single-footer-widget">
                    <h4>Ikuti kami di</h4>
                    <a onclick="window.open('https://www.instagram.com/haryajayalas/', '_blank')" style="cursor: pointer;"><i class="fab fa-instagram fa-3x"></i></a>
                    &emsp;
                    <i class="fab fa-facebook fa-3x"></i>
                    &emsp;
                    <a onclick="window.open('https://api.whatsapp.com/send?phone=6281219263118&text=Halo%20Saya%20ingin%20pesan','_blank')" style="cursor: pointer;"><i class="fab fa-whatsapp fa-3x"></i></a>
                </div>
        </div>
        <div class="footer-bottom row align-items-center text-center text-lg-left no-gutters">
            <p class="footer-text m-0 col-lg-8 col-md-12"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
            <div class="col-lg-4 col-md-12 text-center text-lg-right footer-social">
            </div>
        </div>
    </div>
</footer>
<!-- ================ End footer Area ================= -->






<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="<?php echo base_url('assets/js/jquery-2.2.4.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/popper.js')?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
<script type="text/javascript">
  let modalId = $('#image-gallery');

$(document)
  .ready(function () {

    loadGallery(true, 'a.thumbnail');

    //This function disables buttons when needed
    function disableButtons(counter_max, counter_current) {
      $('#show-previous-image, #show-next-image')
        .show();
      if (counter_max === counter_current) {
        $('#show-next-image')
          .hide();
      } else if (counter_current === 1) {
        $('#show-previous-image')
          .hide();
      }
    }

    /**
     *
     * @param setIDs        Sets IDs when DOM is loaded. If using a PHP counter, set to false.
     * @param setClickAttr  Sets the attribute for the click handler.
     */

    function loadGallery(setIDs, setClickAttr) {
      let current_image,
        selector,
        counter = 0;

      $('#show-next-image, #show-previous-image')
        .click(function () {
          if ($(this)
            .attr('id') === 'show-previous-image') {
            current_image--;
          } else {
            current_image++;
          }

          selector = $('[data-image-id="' + current_image + '"]');
          updateGallery(selector);
        });

      function updateGallery(selector) {
        let $sel = selector;
        current_image = $sel.data('image-id');
        $('#image-gallery-title')
          .text($sel.data('title'));
        $('#image-gallery-image')
          .attr('src', $sel.data('image'));
        disableButtons(counter, $sel.data('image-id'));
      }

      if (setIDs == true) {
        $('[data-image-id]')
          .each(function () {
            counter++;
            $(this)
              .attr('data-image-id', counter);
          });
      }
      $(setClickAttr)
        .on('click', function () {
          updateGallery($(this));
        });
    }
  });

// build key actions
$(document)
  .keydown(function (e) {
    switch (e.which) {
      case 37: // left
        if ((modalId.data('bs.modal') || {})._isShown && $('#show-previous-image').is(":visible")) {
          $('#show-previous-image')
            .click();
        }
        break;

      case 39: // right
        if ((modalId.data('bs.modal') || {})._isShown && $('#show-next-image').is(":visible")) {
          $('#show-next-image')
            .click();
        }
        break;

      default:
        return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
  });
</script>
<script src="<?php echo base_url('assets/js/jquery.ajaxchimp.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/waypoints.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/contact.js')?>"></script>
<script src="<?php echo base_url('assets/js/jquery.form.js')?>"></script>
<script src="<?php echo base_url('assets/js/jquery.validate.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/theme.js')?>"></script>

</body>
</html>