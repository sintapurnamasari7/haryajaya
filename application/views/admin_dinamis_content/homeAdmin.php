        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="row top_tiles">
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-cube"></i></div>
                  <div class="count"><?php echo $jml_pemesanan?></div>
                  <h3>Jumlah Pesanan</h3>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-photo"></i></div>
                  <div class="count"><?php echo $jml_galeri?></div>
                  <h3>Jumlah Galeri</h3>
                </div>
              </div>
            </div>            
          </div>
        </div>