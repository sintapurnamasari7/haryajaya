<div class="right_col" role="main">
          <div class="">
            <div class="page-title"></div>
			<?php if($this->session->flashdata("success")!=null){?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					<strong>Data Berhasil Diubah!</strong>
				</div>
			<?php }elseif($this->session->flashdata("failed")!=null){?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					<strong>Data Gagal Diubah!</strong>
				</div>
			<?php } ?>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Pesanan </h2>                    
                    <div class="clearfix"></div>
                  </div>                  
                  <div class="x_content">
                    <table id="datatable" class="table table-striped table-bordered" >
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Nama Pemesan</th>
                          <th>No Telepon</th>
                          <th>Nomor WA</th>
                          <th>Email</th>
                          <th>Alamat</th>
                          <th>Tanggal Pesan</th>
                          <th>Kategori</th>
                          <th>Status</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
						<?php $no=1;
						foreach($pemesanan as $row){ ?>
                        <tr>
                          <td><?php echo $no++?></td>
                          <td><?php echo $row->nama_pemesan?></td>
                          <td><?php echo $row->no_telp?></td>
						  <td><?php echo $row->no_wa?></td>
						  <td><?php echo $row->email?></td>
						  <td><?php echo $row->alamat?></td>
						  <td><?php echo date('d F Y', strtotime($row->tgl_pemesanan))?></td>
						  <td><?php echo $row->namakategori?></td>
						  <td><?php echo $row->status_pemesanan?></td>
                          <td>
						  <?php if($row->status_pemesanan=='Selesai'){ ?>
							  
						  <?php }else{ ?>
						  <div class="btn-group">
                              <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Edit
                                <span class="caret"></span></button>
                              <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo base_url('Welcome/editPemesanan/'.$row->id_pemesanan.'/1')?>">Sedang diproses</a></li>
                                <li><a href="<?php echo base_url('Welcome/editPemesanan/'.$row->id_pemesanan.'/2')?>">Selesai</a></li>
                              </ul>
                            </div>
						  <?php } ?>
                          </td>
                        </tr>
						<?php }?>
                      </tbody>
                    </table>
					
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>