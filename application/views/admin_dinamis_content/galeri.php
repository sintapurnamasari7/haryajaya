<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Galeri </h2>                    
                    <div class="clearfix"></div>
                  </div>
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add-galeri" data-backdrop="static">Tambah Galeri</button>
                  <div class="x_content">                    
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Nama</th>
                          <th>Kategori</th>
                          <th>Tanggal Upload</th>
                          <th>Gambar</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
						<?php $no=1;
						foreach($galeri as $row){ ?>
                        <tr>
                          <td><?php echo $no++?></td>
                          <td><?php echo $row->nama?></td>
						  <td><?php echo $row->namakategori?></td>
						  <td><?php echo date('d F Y', strtotime($row->tglupload))?></td>
						  <td><img width="100px" src="<?php echo base_url('galeri/'. $row->gambar);?>" /></td>
                          <td><button onclick="addIdGaleri(<?= $row->kode?>)" class="btn btn-success" data-target="#edit-galeri" data-toggle="modal"><i class="glyphicon glyphicon-pencil"></i></button></td>
                        </tr>
						<?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div id="add-galeri" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Tambah Galeri</h4>
              </div>
			  <form class="form-horizontal form-label-left" method="post" action="<?php echo base_url('Welcome/addGaleri')?>" enctype="multipart/form-data">
              <div class="modal-body">
                  <div class="form-group">
                    <label class="control-label col-md-2" for="last-name">Kategori <span class="required">*</span>
                    </label>
                    <div class="col-md-8 col-sm-9 col-xs-12">
                      <select class="form-control" name="kategori" id="kategori">
							<option>-- Pilih Kategori --</option>
                        <?php foreach($kategori as $row){?>
							<option value="<?php echo $row->idkategori?>"><?php echo $row->namakategori?></option>
						<?php } ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-2" for="last-name">Nama <span class="required">*</span>
                    </label>
                    <div class="col-md-8">
                      <input type="text" id="nama" name="nama" required="required" class="form-control col-md-8 col-xs-10">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-2" for="last-name">Gambar <span class="required">*</span>
                    </label>
                    <div class="col-md-8">
                      <input type="file" id="gambar" name="gambar" required="required" class="form-control col-md-8 col-xs-10" accept="image/*">
                      <p>*Ukuran Maks Foto 1 Mb</p>
                    </div>
                  </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
			  </form>
            </div>
          </div>
        </div>
         <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" id="edit-galeri">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Edit Galeri</h4>
              </div>
        <form class="form-horizontal form-label-left" method="post" action="<?php echo base_url('Welcome/editGaleri')?>" enctype="multipart/form-data">
		 <input type="hidden" id="edit-id" name="kode">
              <div class="modal-body">                  
                  <div class="form-group">
                    <label class="control-label col-md-2" for="last-name">Nama <span class="required">*</span>
                    </label>
                    <div class="col-md-8">
                      <input type="text" id="edit-nama" name="edit-nama" required="required" class="form-control col-md-8 col-xs-10">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-2" for="last-name">Gambar
                    </label>
                    <div class="col-md-8">
                      <input type="file" name="edit-gambar" id="gambaredit" class="form-control col-md-8 col-xs-10" accept="image/*">
                      <p>*Ukuran Maks Foto 1 Mb</p>
					  <img src="" id="edit-gambar" width="113" height="113">
					  <p id="label-gambar"></p>
                    </div>
                  </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>
        </form>
            </div>
          </div>
        </div>

        <script src="<?php echo base_url('assets/admin/vendors/jquery/dist/jquery.min.js')?>"></script>
        <script type="text/javascript">
          function addIdGaleri(id){
            $.ajax({  
                      url : "<?php echo base_url(); ?>Welcome/showOneGaleri/"+id, 
                      type: "GET",
                success: function(data) { 
                  console.log(data);
                  
                  var obj = JSON.parse(data);
                  console.log(obj);
                  
                  var kode, nama, gambar;
                  for(var i = 0; i < obj.length; i++){
                    kode = obj[i].kode;
                    nama = obj[i].nama;
                    gambar = obj[i].gambar;
                  }
                  
                  var file = new File([""], gambar);

                  $('#edit-id').val(kode);
                  $('#edit-nama').val(nama);
                  $('#label-gambar').text(gambar);
                  $('#edit-gambar').attr("src", "<?php echo base_url()?>galeri/"+gambar);
                  
                          }
                  });
          };

          function readURL(input) {
              if (input.files && input.files[0]) {
                  var reader = new FileReader();
                  console.log(input.files[0]);
                  
                  reader.onload = function (e) {
                      $('#edit-gambar').attr('src', e.target.result);
                      $('#label-gambar').text(input.files[0].name);

                  }
                  reader.readAsDataURL(input.files[0]);
              }
          }

          $('#gambaredit').change(function(){
            readURL(this);
          });
        </script>