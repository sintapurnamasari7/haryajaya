    <style type="text/css">

    .service-area .single-service .service-content a:after{
        content: none;
    }
    .service-area .single-service .service-content a{
        margin-bottom: 5px;
    }

    </style>
    <section class="home_banner_area">
        <div class="banner_inner d-flex align-items-center">
            <div class="overlay" style="background: url('<?php echo base_url("assets/img/banner/2815186.jpg")?>') no-repeat scroll center center"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 offset-lg-6 col-xl-5 offset-xl-7">
                        <div class="banner_content">
                            <h3>Harya Jaya</h3>
                            <p>Merupakan bengkel las yang berada di kawasan Matraman Jakarta Timur, yang menyediakan jasa pembuatan aksesoris rumah seperti canopi, teralis, rolling door, folding gate, pagar, pintu, konstruksi baja dan lain-lain.</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!--================End Home Banner Area =================-->

    <!--================Service  Area =================-->
    <section class="service-area area-padding">
        <div class="container">
            <div class="row">
                <!-- Single service -->
                <div class="col-md-6 col-lg-4">
                    <div class="single-service">

                        <div class="service-icon">
                            <p class="mb-20" style="font-size: 20pt; color: black">Canopi</p>
                        </div>

                        <div class="service-content">
                            <img src="<?php echo base_url('assets/img/canopi.jpg')?>">
                            <p>Merupakan suatu media penghalang cuaca demi melindungi barang-barang berharga yang berada di luar ruangan atau di halaman rumah anda. kanopi yang digunakan dalam arsitektur saat ini merupakan sejenis atap untuk melindungi bagian luar rumah dari panas matahari dan terpaan hujan.</p>
                            <a href="<?php echo base_url('Welcome/produk/CAN')?>" class="genric-btn default-border radius">Lihat Model Lain</a>
                            <a href="<?php echo base_url('Welcome/pemesanan')?>" class="genric-btn default-border radius">Pesan Sekarang</a>
                        </div>
                    </div>
                </div>

                <!-- Single service -->
                <div class="col-md-6 col-lg-4">
                    <div class="single-service">
                        <div class="service-icon">
                            <p class="mb-20" style="font-size: 20pt; color: black">Teralis</p>
                        </div>
                        <div class="service-content">
                            <img src="<?php echo base_url('assets/img/teralis.jpg')?>">
                            <p>Teralis merupakan konstruksi besi yang berfungsi selain sebagai dekorasi rumah juga sebagai pengaman yang dipasang di jendela maupun di pintu rumah.</p>
                            <a href="<?php echo base_url('Welcome/produk/TER')?>" class="genric-btn default-border radius">Lihat Model Lain</a>
                            <a href="<?php echo base_url('Welcome/pemesanan')?>" class="genric-btn default-border radius">Pesan Sekarang</a>
                        </div>
                    </div>
                </div>


                <!-- Single service -->
                <div class="col-md-6 col-lg-4">
                    <div class="single-service">
                        <div class="service-icon">
                            <p style="font-size: 16pt; color: black">Folding Gate</p>
                        </div>
                        <div class="service-content">
                            <img src="<?php echo base_url('assets/img/rolling.jpg')?>">
                            <p> Pintu besi sistem buka tutupnya digeser kesamping baik arah kekanan maupun arah kekiri, pintu folding gate pada umumnya dipergunakan untuk pintu toko atau pintu garasi. Dari sisi harga dapat dilihat dari sisi ketebalan daun dan ketebalan rangkanya masing – masing.</p>
                            <a href="<?php echo base_url('Welcome/produk/FOL')?>" class="genric-btn default-border radius">Lihat Model Lain</a>
                            <a href="<?php echo base_url('Welcome/pemesanan')?>" class="genric-btn default-border radius">Pesan Sekarang</a>
                        </div>
                    </div>
                </div>

                <!-- Single service -->
                <div class="col-md-6 col-lg-4">
                    <div class="single-service">
                        <div class="service-icon">
                            <p style="font-size: 16pt; color: black">Konstruksi Baja</p>
                        </div>
                        <div class="service-content">
                            <img src="<?php echo base_url('assets/img/baja.jpg')?>">
                            <p>Merupakan suatu kegiatan membangun sarana maupun prasarana. Dalam sebuah bidang arsitektur atau teknik sipil, sebuah konstruksi juga dikenal sebagai bangunan atau satuan infrastruktur pada sebuah area atau pada beberapa area.</p>
                            <a href="<?php echo base_url('Welcome/produk/KON')?>" class="genric-btn default-border radius">Lihat Model Lain</a>
                            <a href="<?php echo base_url('Welcome/pemesanan')?>" class="genric-btn default-border radius">Pesan Sekarang</a>
                        </div>
                    </div>
                </div>

                <!-- Single service -->
                <div class="col-md-6 col-lg-4" style="margin-top: 50px;">
                    <div class="single-service">
                        <div class="service-icon">
                            <p class="mb-20" style="font-size: 20pt; color: black">Pagar</p>
                        </div>
                        <div class="service-content">
                            <img src="<?php echo base_url('assets/img/pagar.jpg')?>">
                            <p>Sebuah konstruksi baja yang berguna untuk membati seatu tempat dengan tempat yang lain. Bahan dan desain nya tergantung dari kebutuhan pembuatan pagar.</p>
                            <a href="<?php echo base_url('Welcome/produk/PAG')?>" class="genric-btn default-border radius">Lihat Model Lain</a>
                            <a href="<?php echo base_url('Welcome/pemesanan')?>" class="genric-btn default-border radius">Pesan Sekarang</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4">
                    <div class="single-service">
                        <div class="service-icon">
                            <p class="mb-20" style="font-size: 16pt; color: black">Rolling Door</p>
                        </div>
                        <div class="service-content">
                            <img src="<?php echo base_url('assets/img/folding.jpg')?>">
                            <p>Pintu yang berfungsi sebagai pengaman dari suatu bangunan, melindungi isi dalam bangunan dari berbagai resiko yang mengancam keamanan.</p>
                            <a href="<?php echo base_url('Welcome/produk/ROL')?>" class="genric-btn default-border radius">Lihat Model Lain</a>
                            <a href="<?php echo base_url('Welcome/pemesanan')?>" class="genric-btn default-border radius">Pesan Sekarang</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================Service Area end =================-->


    <!--================About  Area =================-->
    <!--================About Area End =================-->

    <!--================Feature  Area =================-->
    
    <!--================Feature Area End =================-->


    <!--================About  Area =================-->
    <!--================About Area End =================-->
