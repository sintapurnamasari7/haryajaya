    <style type="text/css">
        .img-thumbnail {
        }
    </style>

    <section class="hero-banner d-flex align-items-center">
        <div class="container text-center">
            <h2>Galeri</h2>
            <nav aria-label="breadcrumb" class="banner-breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url('Welcome/index')?>">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Galeri</li>
                </ol>
            </nav>
        </div>
    </section>

    <div class="container">
      <div class="row">
        <div class="row">
		<?php foreach($galeri as $row){?>
                <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                       data-image="<?php echo base_url('galeri/'. $row->gambar);?>"
                       data-target="#image-gallery">
                        <img class="img-thumbnail"
                             src="<?php echo base_url('galeri/'. $row->gambar);?>"
                             alt="Another alt text">
                    </a>
                    <h6 style="padding: 5px; padding-bottom: 0;"><?php echo $row->nama?></h6>
                    <a href="<?php echo base_url('Welcome/pemesanan')?>" class="genric-btn default circle">Pesan Sekarang</a>
                </div>
		<?php }?>
        </div>


          <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="image-gallery-title"></h4>
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <img id="image-gallery-image" class="img-responsive col-md-12" src="">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary float-left" id="show-previous-image"><i class="fa fa-arrow-left"></i>
                            </button>

                            <button type="button" id="show-next-image" class="btn btn-secondary float-right"><i class="fa fa-arrow-right"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
      </div>
    </div>  