	<style>
		.responsiveContainer{
			position:relative;
			padding-bottom:56%;
			height:0;
			overflow:hidden;
		}
		.responsiveContainer iframe{
			position:absolute;
			top:0;
			left:0;
			widht:100%;
			height:100%;
		}
	</style>
   <!--================Hero Banner Area Start =================-->
    <section class="hero-banner d-flex align-items-center">
        <div class="container text-center">
            <h2>Tentang</h2>
            <nav aria-label="breadcrumb" class="banner-breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url('Welcome/index')?>">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Tentang</li>
                </ol>
            </nav>
        </div>
    </section>
    <!--================Hero Banner Area End =================-->


  <!-- ================ contact section start ================= -->
  <section class="contact-section area-padding">
    <div class="container">
        <div class="responsiveContainer" >
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7932.844548793116!2d106.863295!3d-6.2079006!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f4879caddab5%3A0x16552311849a74cf!2sHarya%20Jaya!5e0!3m2!1sen!2sid!4v1567060156810!5m2!1sen!2sid" width="1100" height="480" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>


      <div class="row">
        <div class="col-12">
          <h2 class="contact-title">Profil</h2>
        </div>
        <div class="col-lg-8">
           <div class="media contact-info">
            <div class="media-body">
              <p>Berdiri sejak tahun 2008, namun pemilik dari bengkel Harya Jaya telah berpengalaman dibidang las sejak tahun 1988. Kami melayani perbaikan maupun pembuatan aksesoris rumah maupun seperti teralis, canopi, pagar, pintu, reling tangga, rolling door, folding gate, konstruksi baja dan lain-lain. Dengan memilih bahan dengan kualitas bagus dan tentunya sesuai dengan keinginan konsumen, sehingga menghasilkan produk yang berkualitas.</p>
            </div>
          </div>
            
            
        </div>

        <div class="col-lg-4">
          <div class="media contact-info">
            <span class="contact-info__icon"><i class="ti-home"></i></span>
            <div class="media-body">
              <h3>Jalan Kelapa Sawit Raya No. 19 RT. 12/8</h3>
              <p>Kayu Manis, Jakarta Timur, 13130.</p>
            </div>
          </div>
          <div class="media contact-info">
            <span class="contact-info__icon"><i class="ti-tablet"></i></span>
            <div class="media-body">
              <h3><a href="tel:081219263118">+62 8129263118</a></h3>
              <p>Buka setiap hari 08.00 - 17.00 kecuali hari besar.</p>
            </div>
          </div>
          <div class="media contact-info">
            <span class="contact-info__icon"><i class="ti-email"></i></span>
            <div class="media-body">
              <h3><a href="mailto:haryajayalas@gmail.com">haryajayalas@gmail.com</a></h3>              
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- ================ contact section end ================= -->