    <script type="text/javascript">var baseurl = "<?php echo base_url(); ?>";</script>
	<section class="hero-banner d-flex align-items-center">
        <div class="container text-center">
            <h2>Pemesanan</h2>
            <nav aria-label="breadcrumb" class="banner-breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url('Welcome/index')?>">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Pemesanan</li>
                </ol>
            </nav>
        </div>
    </section>
	<style>
	input[type=number]::-webkit-inner-spin-button, 
	input[type=number]::-webkit-outer-spin-button { 
		-webkit-appearance: none;
		-moz-appearance: none;
		appearance: none;
		margin: 0; 
	}
	</style>
  <section class="contact-section area-padding">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 class="contact-title">Form Pemesanan</h2>
          <p>Isi form di bawah ini kemudian Kami akan menghubungi Anda</p>
        </div>
        <div class="col-lg-12">
          <form class="form-contact contact_form" method="post" id="contactForm" novalidate="novalidate" >
            <div class="row">
              <div class="col-sm-6">                
                <div class="form-group">
                  <h6 class="typo-list">Nama</h6>
                  <input class="form-control" name="nama" id="name1" type="text" placeholder="Masukan nama">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <h6 class="typo-list">Nomor Telepon</h6>
                  <input class="form-control" name="notelp" id="name" type="number" placeholder="Masukan nomor telefon">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <h6 class="typo-list">Nomor WA</h6>
                  <input class="form-control" name="nowa" id="nowa" type="number" placeholder="Masukan nomor WA">
                </div>
              </div>
              <div class="col-6">
                <div class="form-group">
                  <h6 class="typo-list">Email</h6>
                  <input class="form-control" name="email" id="subject" type="text" placeholder="Masukan email">
                </div>
              </div>
              <div class="col-12">
                <div class="form-group">
                    <h6 class="typo-list">Alamat Lengkap</h6>
                    <textarea class="form-control w-100" name="alamat" id="message" cols="20" rows="5" placeholder="Masukan alamat lengkap"></textarea>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <h6 class="typo-list">Kota</h6>
                  <input class="form-control" name="kota" id="kota" type="text" placeholder="Masukan kota">
                </div>
              </div>
              <div class="col-6">
                <div class="form-group">
                  <h6 class="typo-list">Produk yang akan dipesan</h6>
                  <select class="form-control form-control-lg" name="kategori">
                      <option value="">-- Pilih kategori pemesanan --</option>
					  <?php foreach($kategori as $row){?>
							<option value="<?php echo $row->idkategori?>"><?php echo $row->namakategori?></option>
						<?php } ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-12">
            <div class="form-group">
              <button type="submit" class="form-control" style="background:#1c425e; color:white; cursor:pointer;">Send Message</button>
            </div>
            </div>
          </form>
        </div>

        
        </div>
      </div>
    </div>
  </section>
  <!-- ================ contact section end ================= -->

 <!-- ================ start footer Area ================= -->
   

<!--================Contact Success and Error message Area =================-->
    <div id="success" class="modal modal-message fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fas fa-times"></i>
                    </button>
                    <h2>Terima Kasih</h2>
                    <p>Pesanan anda sudah dalam antrian...</p>
					<p>Pihak kami akan segera menghubungi anda.</p>
                </div>
            </div>
        </div>
    </div>

    <!-- Modals error -->

    <div id="error" class="modal modal-message fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fas fa-times"></i>
                    </button>
                    <h2>Maaf Pesanan anda gagal</h2>
                    <p> Segera hubungi kami di Informasi kontak </p>
                </div>
            </div>
        </div>
    </div>